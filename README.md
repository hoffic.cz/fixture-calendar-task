# Fixture Calendar Task

### Prerequisites
* [Docker](https://www.docker.com/) 2.0.0.3

### Installing

run docker:
```
 docker-compose build
```
```
 docker-compose up -d
```
connect to the container:
```
 docker-compose exec php sh
```
run composer
```
 composer install
```

call localhost in your browser:
- [http://localhost:8080](http://localhost:8080)

### Run Unit Tests

connect to the container:
```
 docker-compose exec php sh
```

run tests:

```
 php bin/phpunit
```