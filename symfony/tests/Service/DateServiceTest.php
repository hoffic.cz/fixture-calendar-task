<?php

namespace App\Tests\Service;

use App\Controller\HomeController;
use App\Service\DateService;
use PHPUnit\Framework\TestCase;

class DateServiceTest extends TestCase
{
    const DATE_FORMAT = "d/m/Y";

    /** @var DateService */
    private $dateService;

    public function setUp() {

        parent::setUp();

        $this->dateService = new DateService();
    }

    public function testChristmasAwaitingThisYear()
    {
        $expected = \DateTime::createFromFormat(self::DATE_FORMAT, "24/12/2019");

        $today = \DateTime::createFromFormat(self::DATE_FORMAT, "01/12/2019");
        $actual = $this->dateService->getNextDate(HomeController::CHRISTMAS_DAY, $today);

        $this->assertEquals($expected, $actual);
    }

    public function testChristmasWhenItsNextYear()
    {
        $expected = \DateTime::createFromFormat(self::DATE_FORMAT, "24/12/2020");

        $today = \DateTime::createFromFormat(self::DATE_FORMAT, "25/12/2020");
        $actual = $this->dateService->getNextDate(HomeController::CHRISTMAS_DAY, $today);

        $this->assertEquals($expected, $actual);
    }

    public function testChristmasWhenItsToday()
    {
        $expected = \DateTime::createFromFormat(self::DATE_FORMAT, "24/12/2019");

        $today = \DateTime::createFromFormat(self::DATE_FORMAT, "24/12/2019");
        $actual = $this->dateService->getNextDate(HomeController::CHRISTMAS_DAY, $today);

        $this->assertEquals($expected, $actual);
    }

    public function testChristmasAwaitingThisYearCountDown()
    {
        $expected = 1;

        $today = \DateTime::createFromFormat(self::DATE_FORMAT, "23/12/2019");
        $nextChristmasDay = $this->dateService->getNextDate(HomeController::CHRISTMAS_DAY, $today);
        $actual = $this->dateService->getDaysLeft($nextChristmasDay, $today);

        $this->assertEquals($expected, $actual);
    }

    public function testChristmasWhenItsNextYearCountDown()
    {
        $expected = 365;

        $today = \DateTime::createFromFormat(self::DATE_FORMAT, "25/12/2019");
        $nextChristmasDay = $this->dateService->getNextDate(HomeController::CHRISTMAS_DAY, $today);
        $actual = $this->dateService->getDaysLeft($nextChristmasDay, $today);

        $this->assertEquals($expected, $actual);
    }

    public function testChristmasWhenItsTodayCountDown()
    {
        $expected = 0;

        $today = \DateTime::createFromFormat(self::DATE_FORMAT, "24/12/2019");
        $nextChristmasDay = $this->dateService->getNextDate(HomeController::CHRISTMAS_DAY, $today);
        $actual = $this->dateService->getDaysLeft($nextChristmasDay, $today);

        $this->assertEquals($expected, $actual);
    }
}