<?php

namespace App\Controller;

use App\Service\DateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    const MY_BIRTHDAY = "24/07";
    const CHRISTMAS_DAY = "24/12";

    /**
     * @Route("/", name="home")
     */
    public function index(DateService $dateService)
    {
        $today = new \DateTime();
        $nextBirthday = $dateService->getNextDate(self::MY_BIRTHDAY, $today);
        $nextChristmasDay = $dateService->getNextDate(self::CHRISTMAS_DAY, $today);

        return $this->render('index.html.twig', [
            "nextBirthday" => $nextBirthday,
            "nextChristmas" => $nextChristmasDay,
            "nextBirthdayDaysLeft" => $dateService->getDaysLeft($nextBirthday, $today),
            "nextChristmasDaysLeft" => $dateService->getDaysLeft($nextChristmasDay, $today)
        ]);
    }


}