<?php

namespace App\Service;

class DateService
{
    public function getNextDate(string $date, \DateTime $today): \DateTime
    {
        $nextDate = \DateTime::createFromFormat("d/m", $date);
        if ($today <= $nextDate) {
            return $nextDate;
        }

        return $nextDate->modify("+ 1 year");
    }

    public function getDaysLeft(\DateTime $date, \DateTime $today): int
    {
        return (int) $today->diff($date)->format('%a');
    }
}